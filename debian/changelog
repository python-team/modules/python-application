python-application (1.4.1-2) UNRELEASED; urgency=medium

  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove ancient X-Python-Version field
  * Remove debian/pycompat, it's not used by any modern Python helper
  * Convert git repository from git-dpm to gbp layout
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Ondřej Nový <novy@ondrej.org>  Tue, 29 Mar 2016 21:55:08 +0200

python-application (1.4.1-1) unstable; urgency=low

  * New upstream version.

 -- Bernd Zeimetz <bzed@debian.org>  Sun, 05 Oct 2014 13:22:20 +0200

python-application (1.3.0-1) unstable; urgency=low

  * New upstream release.

 -- Bernd Zeimetz <bzed@debian.org>  Thu, 26 Apr 2012 20:30:41 +0200

python-application (1.2.8-1) unstable; urgency=low

  * New upstream release.

 -- Bernd Zeimetz <bzed@debian.org>  Mon, 04 Jul 2011 16:21:48 +0200

python-application (1.2.6-1) unstable; urgency=low

  * New upstream release.
  * Migrate to dh and dh_python2.

 -- Bernd Zeimetz <bzed@debian.org>  Wed, 16 Feb 2011 21:53:59 +0100

python-application (1.2.5-1) unstable; urgency=low

  * New upstream release.

 -- Bernd Zeimetz <bzed@debian.org>  Mon, 14 Feb 2011 14:57:53 +0100

python-application (1.2.4-1) unstable; urgency=low

  * New upstream release.

 -- Bernd Zeimetz <bzed@debian.org>  Tue, 13 Apr 2010 21:18:37 +0200

python-application (1.2.3-1) unstable; urgency=low

  * New upstream release

 -- Bernd Zeimetz <bzed@debian.org>  Thu, 25 Feb 2010 02:39:50 +0100

python-application (1.2.2-1) unstable; urgency=low

  * New upstream release

 -- Bernd Zeimetz <bzed@debian.org>  Tue, 02 Feb 2010 21:08:00 +0100

python-application (1.2.1-1) unstable; urgency=low

  * New upstream release.
  * Dropping the twisted Suggests, not needed anymore.
  * Requiring Python 2.5.
  * Dropping python-zopeinterface build-dep.
  * Bumping Standards-Version, so changes needed.

 -- Bernd Zeimetz <bzed@debian.org>  Fri, 18 Dec 2009 14:11:30 +0100

python-application (1.1.5-1) unstable; urgency=low

  * New upstream release.

 -- Bernd Zeimetz <bzed@debian.org>  Tue, 11 Aug 2009 11:38:45 +0200

python-application (1.1.4-1) unstable; urgency=low

  * New upstream release.
  * Updating long description with new features.

 -- Bernd Zeimetz <bzed@debian.org>  Mon, 20 Jul 2009 14:09:47 +0200

python-application (1.1.3-1) unstable; urgency=low

  * New upstream version.
  * Bump Standards-Version to 3.8.2, no changes needed.
  * Enhance long descriptions with new features (version number
    management)

 -- Bernd Zeimetz <bzed@debian.org>  Wed, 15 Jul 2009 16:37:53 +0200

python-application (1.1.1-1) unstable; urgency=low

  * New upstream version.
  * debian/control:
    - Bumping Standards-Version to 3.8.1, no changes needed.

 -- Bernd Zeimetz <bzed@debian.org>  Sun, 12 Apr 2009 01:50:37 +0200

python-application (1.1.0-1) unstable; urgency=low

  [ Bernd Zeimetz ]
  * New upstream release
  * debian/watch:
    - Simplifying url
  *  debian/control:
    - Updating my email address.
    - Adding missing misc-depends

  [ Sandro Tosi ]
  * debian/control
    - uniforming Vcs-Browser field
  * debian/control
    - switch Vcs-Browser field to viewsvn

 -- Bernd Zeimetz <bzed@debian.org>  Sun, 15 Feb 2009 23:12:04 +0100

python-application (1.0.9-4) unstable; urgency=low

  * debian/copyright:
    - Fixing the 'downloaded from' link, thanks to Jan Niehusmann.
      (Closes: #450613)

 -- Bernd Zeimetz <bernd@bzed.de>  Thu, 15 Nov 2007 23:05:30 +0100

python-application (1.0.9-3) unstable; urgency=low

  * debian/control:
    - Adding Homepage field, removing pseudo-field from description
    - Fixing a couple of spelling errors (Closes: #444047), thanks to
      Joseph Dow
    - Renaming XS-Vcs-* fields to Vcs-*, as they're supported by dpkg now

 -- Bernd Zeimetz <bernd@bzed.de>  Thu, 04 Oct 2007 20:36:21 +0200

python-application (1.0.9-2) unstable; urgency=low

  * debian/control:
    - Moving python-twisted-core from Recommends to Depends, as 90% of the
      package need it to work.

 -- Bernd Zeimetz <bernd@bzed.de>  Sun, 09 Sep 2007 16:22:17 +0200

python-application (1.0.9-1) unstable; urgency=low

  * New upstream version

 -- Bernd Zeimetz <bernd@bzed.de>  Sat, 08 Sep 2007 17:17:38 +0200

python-application (1.0.8-1) unstable; urgency=low

  * Initial release (Closes: #440482)

 -- Bernd Zeimetz <bernd@bzed.de>  Mon, 03 Sep 2007 01:50:24 +0200
